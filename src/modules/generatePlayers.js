import randomName from 'random-name';

function generatePlayers(count) {
    const players = [];

    for (let i = 0; i < count; i++) {
        players.push({
            name: `${randomName.first()} ${randomName.last()}`,
            id: `player-${i}`,
            inGame: false,
        });
    }

    return players;
}

function generateMatches(players, matchSize = 2) {
    const matches = [];

    if (Array.isArray(players)) {

        for (let i = 0; i < Math.ceil(players.length / matchSize); i++) {
            matches[i] = players.slice((i * matchSize), (i * matchSize) + matchSize)
        }

        return matches.map((item, index) => ({
            id: `match-${index}`,
            players: item,
        }));
    }
    return matches;
}

export { generatePlayers, generateMatches };