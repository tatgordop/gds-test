import Vue from 'vue'
import App from './App.vue'

import { Drag, Drop } from 'vue-drag-drop';

Vue.component('drag', Drag);
Vue.component('drop', Drop);

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
}).$mount('#app')